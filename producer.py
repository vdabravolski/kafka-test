# !/usr/bin/env python
import threading, logging, time
import config
import base64
import cv2

from kafka import KafkaProducer


class Producer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()

        self.producer = KafkaProducer(bootstrap_servers='{0}:{1}'.format(config.broker_host, config.broker_port))

    def stop(self):
        self.stop_event.set()
        self.producer.close()

    def send_message(self):
        self.producer.send(config.topic, b"sample test")


    def send_image_message(self):
        """
        This function takes image and send it to kafka in binary format. The path to image is defined in config.
        """

        with open(config.img_message, 'rb') as imageFile:
            enc_im = base64.b64encode(imageFile.read())
            self.producer.send(config.images_topic, enc_im)


    def stream_video(self):
        video = cv2.VideoCapture(config.video_message)
        print(' emitting.....')

        i = 1 # frame counter
        # read the file
        while (video.isOpened):
            # read the image in each frame
            success, image = video.read()
            # check if the file has read to the end
            if not success:
                break
            # convert the image png
            ret, jpeg = cv2.imencode('.png', image)
            # Convert the image to bytes and send to kafka
            self.producer.send(config.video_topic, jpeg.tobytes())
            # To reduce CPU usage create sleep time of 0.2sec
            time.sleep(0.05)
            print("Sending frame index {} ...".format(i))
            i += 1
        # clear the capture
        video.release()
        print('done emitting')




if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
    )
    producer = Producer()

    iter = 1
    count = 0
    while count < iter:
        if config.stream_video:
            producer.stream_video()
        else:
            producer.send_image_message()
        time.sleep(1)
        count += 1

    producer.stop()
