from flask import Flask, Response
from kafka import KafkaConsumer
import config

#connect to Kafka server and pass the topic we want to consume
consumer = KafkaConsumer(config.video_topic,
                         bootstrap_servers='{0}:{1}'.format(config.broker_host, config.broker_port))
#Continuously listen to the connection and print messages as recieved
app = Flask(__name__)

@app.route('/')
def index():
    # return a multipart response
    return Response(kafkastream(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
def kafkastream():
    for msg in consumer:
        yield (b'--frame\r\n'
               b'Content-Type: image/png\r\n\r\n' + msg.value + b'\r\n\r\n')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)