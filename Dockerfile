FROM ubuntu:latest
MAINTAINER vadim dabravolski "vadim.dabravolski@gmail.com"
RUN apt-get update
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
RUN pip install --upgrade pip
COPY . /kafka-test
WORKDIR /kafka-test
RUN pip install -r requirements.txt
# ENTRYPOINT ["python"]
# CMD ["producer.py"]
