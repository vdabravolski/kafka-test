#kafka configuration

# The section below should be configured for each particular environment.
# Please make sure that you have a running instance of kafka or kafka cluster.
broker_host = "192.168.99.100"
broker_port = "32400"

zookeeper_host = "0.0.0.0"
zookeeper_port = "2181"

topic = "test-topic"
images_topic = "images"
video_topic = "videos"

img_message = "outbox/nn.png"
video_message = "outbox/video_sample.mp4"
stream_video = True # flag to stream video.
                    # If not, then only images will be published.
inbox_dir = "inbox"