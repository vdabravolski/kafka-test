<h2>Spin up environment </h2>
If you'd like to run tests on Docker container, then:
1. Build docker image: ```docker build --rm -f Dockerfile -t kafka-test:latest .```
2. Run container: ```docker run --rm -it kafka-test:latest```
3. Once container is running, update config.py to point to running instance of kafka

<h2>Use cases</h2>
<h3> Publish images to Kafka and read back </h3>
1. Run ```python3 producer.py```
3. Run ```python3 image-consumer.py``` in separate console
4. Check that in inbox folder received images are stored.

<h3> Stream video </h3>
1. Update config.py to set ```stream_video=True ```
2. Run ```python3 producer.py"```
3. Run ```python3 video-consumer.py"```
4. Check video stream on local server: ```http://0.0.0.0:5000```
