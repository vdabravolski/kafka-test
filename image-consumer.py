# !/usr/bin/env python
import threading, logging, time
import multiprocessing
import config
import os
import base64

from kafka import KafkaConsumer


class imageConsumer(multiprocessing.Process):
    def __init__(self):
        multiprocessing.Process.__init__(self)
        self.stop_event = multiprocessing.Event()

        self.consumer = KafkaConsumer(bootstrap_servers='{0}:{1}'.format(config.broker_host, config.broker_port),
                                 auto_offset_reset='earliest',
                                 consumer_timeout_ms=1000)
        self.consumer.subscribe([config.images_topic])

    def stop(self):
        self.stop_event.set()

    def poll_images(self):
        while not self.stop_event.is_set():
            for message in self.consumer:

                if not os.path.exists(config.inbox_dir):
                    os.makedirs(config.inbox_dir)

                img_file = open(config.inbox_dir+"/image_{0}.png".format(message.timestamp), 'wb')
                img_file.write(base64.decodebytes(message.value))
                print("receiving image ...")

                if self.stop_event.is_set():
                    break
        self.consumer.close()


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
    )
    consumer = imageConsumer()
    consumer.poll_images()
    consumer.stop()
